#!/bin/bash

REF=${CI_COMMIT_TAG:-master}

haxelib run dox -o public -i build/types.xml \
    -in ^linkedlist.*$ \
    -D source-path https://gitlab.com/haath/linkedlist/-/tree/${REF}/src/ \
    -D website https://gitlab.com/haath/linkedlist \
    -D logo https://gitlab.com/uploads/-/system/project/avatar/19075408/haxe-logo-glyph.png?width=64 \
    --title "linkedlist API documentation" \
    -D description  "linkedlist API documentation" \
    -D version ${REF} \
    --toplevel-package linkedlist

