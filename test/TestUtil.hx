import utest.Assert;
import haxe.PosInfos;
import types.Item;
import linkedlist.LinkedList;


class TestUtil
{
    /**
     * Utility extension function which creates an item with the given value, pushes it to the list,
     * and returns it.
     */
    public static function pushValue(list: LinkedList<Item>, value: Int): Item
    {
        var item: Item = new Item(value);
        list.push(item);
        return item;
    }


    public static function arrayEquals<T>(a: Array<T>, b: Array<T>, ?pos: PosInfos)
    {
        if (!Assert.equals(a.length, b.length, pos))
        {
            return;
        }

        for (i in 0...a.length)
        {
            Assert.equals(a[i], b[i], pos);
        }
    }


    public static function assertList(list: LinkedList<Item>, expected: Array<Int>, ?pos: PosInfos)
    {
        var i: Int = 0;
        var iter: Item = @:privateAccess list.head;

        while (iter != null)
        {
            if (i >= expected.length)
            {
                Assert.fail('expected ${expected.length} items but got more', pos);
                return;
            }

            Assert.equals(expected[i], iter.value, 'expected ${expected[i]} at position $i but got ${iter.value}' ,pos);

            iter = iter.__next;
            i++;
        }

        Assert.equals(expected.length, i, 'expected length ${expected.length} but got $i', pos);
        Assert.equals(expected.length, list.length, 'expected length ${expected.length} but got ${list.length}', pos);
    }


    public static function assertListIterator(list: LinkedList<Item>, expected: Array<Int>, ?pos: PosInfos)
    {
        var i: Int = 0;

        if (expected.length > 0)
        {
            Assert.isFalse(list.isEmpty(), pos);
        }

        for (item in list)
        {
            if (i >= expected.length)
            {
                Assert.fail('expected ${expected.length} items but got more', pos);
                return;
            }

            Assert.equals(expected[i], item.value, 'expected ${expected[i]} at position $i but got ${item.value}' ,pos);
            i++;
        }

        Assert.equals(expected.length, i, 'expected length ${expected.length} but got $i', pos);
        Assert.equals(expected.length, list.length, 'expected length ${expected.length} but got ${list.length}', pos);
    }
}
