package tests;

import types.ExtendedItem;
import linkedlist.LinkedListIterator;
import linkedlist.LinkedListItem;
import haxe.PosInfos;
import types.Item;
import linkedlist.LinkedList;
import utest.Assert;
import utest.ITest;

using TestUtil;


class TestLinkedList implements ITest
{
    public function new() { }


    function testConstructor()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        TestUtil.assertList(list, []);
    }


    function testExtendedList()
    {
        var list: LinkedList<ExtendedItem> = new LinkedList(LIFO);

        // expect no compilation errors
        Assert.pass();
    }


    function testPush()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        list.push(new Item(5));
        list.push(new Item(3));
        list.push(new Item(-2));
        list.push(new Item(8));

        TestUtil.assertList(list, [ 8, -2, 3, 5 ]);
    }


    function testIterator()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        list.push(new Item(5));
        list.push(new Item(3));
        list.push(new Item(-2));
        list.push(new Item(8));

        var iter: Iterator<Item> = @:privateAccess list.iterator();

        Assert.isTrue(iter.hasNext());
        Assert.equals(8, iter.next().value);
        Assert.isTrue(iter.hasNext());
        Assert.equals(-2, iter.next().value);
        Assert.isTrue(iter.hasNext());
        Assert.equals(3, iter.next().value);
        Assert.isTrue(iter.hasNext());
        Assert.equals(5, iter.next().value);

        Assert.isFalse(iter.hasNext());

        TestUtil.assertList(list, [ 8, -2, 3, 5 ]);
    }


    function testIterFIFO()
    {
        var list: LinkedList<Item> = new LinkedList(FIFO);

        list.push(new Item(5));
        list.push(new Item(3));
        list.push(new Item(-2));
        list.push(new Item(8));

        TestUtil.assertListIterator(list, [ 5, 3, -2, 8 ]);
    }


    function testIterReverse()
    {
        var list: LinkedList<Item> = new LinkedList(FIFO);

        list.push(new Item(5));
        list.push(new Item(3));
        list.push(new Item(-2));
        list.push(new Item(8));

        TestUtil.assertListIterator(list, [ 5, 3, -2, 8 ]);

        list.reverse();

        TestUtil.assertListIterator(list, [ 8, -2, 3, 5 ]);

        list.reverse();

        TestUtil.assertListIterator(list, [ 5, 3, -2, 8 ]);
    }


    function testPop()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        // push an item and pop it
        list.push(new Item(5));
        Assert.equals(5, list.pop().value);
        TestUtil.assertListIterator(list, [ ]);

        // push two items and pop them
        list.push(new Item(5));
        list.push(new Item(7));
        Assert.equals(7, list.pop().value);
        TestUtil.assertListIterator(list, [ 5 ]);
        Assert.equals(5, list.pop().value);
        TestUtil.assertListIterator(list, [ ]);
        Assert.isTrue(list.isEmpty());

        // push three items, pop one, and add one more
        list.push(new Item(5));
        list.push(new Item(7));
        list.push(new Item(9));
        TestUtil.assertListIterator(list, [ 9, 7, 5 ]);
        Assert.equals(9, list.pop().value);
        TestUtil.assertListIterator(list, [ 7, 5 ]);
        list.push(new Item(11));
        TestUtil.assertListIterator(list, [ 11, 7, 5 ]);
    }


    function testPopFIFO()
    {
        var list: LinkedList<Item> = new LinkedList(FIFO);

        // push two items and pop them
        list.push(new Item(5));
        list.push(new Item(7));
        Assert.equals(5, list.pop().value);
        TestUtil.assertListIterator(list, [ 7 ]);
        Assert.equals(7, list.pop().value);
        TestUtil.assertListIterator(list, [ ]);
        Assert.isTrue(list.isEmpty());

        // push three items, pop one, and add one more
        list.push(new Item(5));
        list.push(new Item(7));
        list.push(new Item(9));
        TestUtil.assertListIterator(list, [ 5, 7, 9 ]);
        Assert.equals(5, list.pop().value);
        TestUtil.assertListIterator(list, [ 7, 9 ]);
        list.push(new Item(11));
        TestUtil.assertListIterator(list, [ 7, 9, 11 ]);
    }


    function testRemoveHead()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        var tail: Item = list.pushValue(5);
        var middle: Item = list.pushValue(7);
        var head: Item = list.pushValue(9);

        TestUtil.assertListIterator(list, [ 9, 7, 5 ]);

        list.remove(head);

        TestUtil.assertListIterator(list, [ 7, 5 ]);
    }


    function testRemoveTail()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        var tail: Item = list.pushValue(5);
        var middle: Item = list.pushValue(7);
        var head: Item = list.pushValue(9);

        TestUtil.assertListIterator(list, [ 9, 7, 5 ]);

        list.remove(tail);

        TestUtil.assertListIterator(list, [ 9, 7 ]);
    }


    function testRemoveHeadFIFO()
    {
        var list: LinkedList<Item> = new LinkedList(FIFO);

        var tail: Item = list.pushValue(5);
        var middle: Item = list.pushValue(7);
        var head: Item = list.pushValue(9);

        TestUtil.assertListIterator(list, [ 5, 7, 9 ]);

        list.remove(head);

        TestUtil.assertListIterator(list, [ 5, 7 ]);
    }


    function testRemoveTailFIFO()
    {
        var list: LinkedList<Item> = new LinkedList(FIFO);

        var tail: Item = list.pushValue(5);
        var middle: Item = list.pushValue(7);
        var head: Item = list.pushValue(9);

        TestUtil.assertListIterator(list, [ 5, 7, 9 ]);

        list.remove(tail);

        TestUtil.assertListIterator(list, [ 7, 9 ]);
    }


    function testRemoveMiddle()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        var tail: Item = list.pushValue(5);
        var middle: Item = list.pushValue(7);
        var head: Item = list.pushValue(9);

        TestUtil.assertListIterator(list, [ 9, 7, 5 ]);

        list.remove(middle);

        TestUtil.assertListIterator(list, [ 9, 5 ]);
    }


    function testRemoveWhileIteratingIterItem()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        var tail: Item = list.pushValue(5);
        var middle1: Item = list.pushValue(7);
        var middle2: Item = list.pushValue(8);
        var middle3: Item = list.pushValue(9);
        var head: Item = list.pushValue(10);

        var seen: Array<Int> = [];
        for (item in list)
        {
            if (item == middle2 || item == middle3)
            {
                list.remove(item);
            }
            seen.push(item.value);
        }
        TestUtil.arrayEquals([ 10, 9, 8, 7, 5], seen);

        TestUtil.assertListIterator(list, [ 10, 7, 5 ]);
    }


    function testPopWhileIterating()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        var tail: Item = list.pushValue(5);
        var middle1: Item = list.pushValue(7);
        var middle2: Item = list.pushValue(8);
        var middle3: Item = list.pushValue(9);
        var head: Item = list.pushValue(10);

        var iter: Iterator<Item> = list.iterator();
        Assert.equals(head, list.pop());

        Assert.equals(middle3, iter.next());
    }


    function testRemoveLastItem()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        var item: Item = list.pushValue(5);
        list.remove(item);

        Assert.isNull(@:privateAccess list.head);
        Assert.isNull(@:privateAccess list.tail);
    }


    function testPopLastItem()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        var item: Item = list.pushValue(5);
        list.pop();

        Assert.isNull(@:privateAccess list.head);
        Assert.isNull(@:privateAccess list.tail);
    }


    function testRemoveWhileIteratingNextItem()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        var tail: Item = list.pushValue(5);
        var middle1: Item = list.pushValue(7);
        var middle2: Item = list.pushValue(8);
        var middle3: Item = list.pushValue(9);
        var head: Item = list.pushValue(10);

        var seen: Array<Int> = [];
        for (item in list)
        {
            if (item == middle2)
            {
                list.remove(middle1);
            }
            seen.push(item.value);
        }
        TestUtil.arrayEquals([ 10, 9, 8, 5 ], seen);

        TestUtil.assertListIterator(list, [ 10, 9, 8, 5 ]);
    }


    function testRemoveWhileIteratingNextItemFIFO()
    {
        var list: LinkedList<Item> = new LinkedList(FIFO);

        var tail: Item = list.pushValue(5);
        var middle1: Item = list.pushValue(7);
        var middle2: Item = list.pushValue(8);
        var middle3: Item = list.pushValue(9);
        var head: Item = list.pushValue(10);

        var seen: Array<Int> = [];
        for (item in list)
        {
            if (item == middle2)
            {
                list.remove(middle3);
            }
            seen.push(item.value);
        }
        TestUtil.arrayEquals([ 5, 7, 8, 10 ], seen);

        TestUtil.assertListIterator(list, [ 5, 7, 8, 10 ]);
    }


    function testRemoveWhileIteratingOtherItem()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        var tail: Item = list.pushValue(5);
        var middle1: Item = list.pushValue(7);
        var middle2: Item = list.pushValue(8);
        var middle3: Item = list.pushValue(9);
        var head: Item = list.pushValue(10);

        var seen: Array<Int> = [];
        for (item in list)
        {
            if (item == middle3)
            {
                list.remove(middle1);
            }
            seen.push(item.value);
        }
        TestUtil.arrayEquals([ 10, 9, 8, 5], seen);

        TestUtil.assertListIterator(list, [ 10, 9, 8, 5 ]);
    }


    function testRemoveItemNotInList()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        list.push(new Item(5));
        list.push(new Item(7));

        Assert.raises(() -> list.remove(new Item(9)));
    }


    function testRemoveItemTwice()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        var tail: Item = list.pushValue(5);
        var middle: Item = list.pushValue(7);
        var head: Item = list.pushValue(9);

        // first removal without error
        list.remove(middle);

        // second removal should raise exception
        Assert.raises(() -> list.remove(middle));
    }


    function testPopEmptyList()
    {
        var list: LinkedList<Item> = new LinkedList(LIFO);

        Assert.raises(() -> list.pop());
    }


    function testIterNested()
    {
        var list: LinkedList<Item> = new LinkedList(FIFO);

        list.push(new Item(5));
        list.push(new Item(3));
        list.push(new Item(8));

        var seen: Array<Int> = [];
        for (item in list)
        {
            seen.push(item.value);

            for (item in list)
            {
                seen.push(item.value);
            }
        }

        TestUtil.arrayEquals([5, 5, 3, 8, 3, 5, 3, 8, 8, 5, 3, 8], seen);
    }


    function testIterNestedRemove()
    {
        var list: LinkedList<Item> = new LinkedList(FIFO);

        var x1: Item = list.pushValue(5);
        var x2: Item = list.pushValue(3);
        var x3: Item = list.pushValue(8);

        var seen: Array<Int> = [];
        var removed: Bool = false;
        for (item in list)
        {
            seen.push(item.value);

            for (item in list)
            {
                if (item == x2 && !removed)
                {
                    list.remove(x3);
                    removed = true;
                }

                seen.push(item.value);
            }
        }

        TestUtil.arrayEquals([5, 5, 3, 3, 5, 3 ], seen);
    }


    function testIterNestedReverse()
    {
        var list: LinkedList<Item> = new LinkedList(FIFO);

        var x1: Item = list.pushValue(5);
        var x2: Item = list.pushValue(3);
        var x3: Item = list.pushValue(8);

        var seenOuter: Array<Int> = [];
        var seenInner: Array<Int> = [];
        for (item in list)
        {
            seenOuter.push(item.value);

            list.reverse();

            for (item in list)
            {
                seenInner.push(item.value);
            }
        }

        TestUtil.arrayEquals([ 5, 3, 8 ], seenOuter);
        TestUtil.arrayEquals([ 8, 3, 5, 5, 3, 8, 8, 3, 5 ], seenInner);
    }


    function testContains()
    {
        var list: LinkedList<Item> = new LinkedList(FIFO);

        var x1: Item = list.pushValue(5);
        var x2: Item = list.pushValue(3);
        var x3: Item = list.pushValue(8);

        Assert.isTrue(list.contains(x1));
        Assert.isTrue(list.contains(x2));
        Assert.isTrue(list.contains(x3));

        list.remove(x2);

        Assert.isTrue(list.contains(x1));
        Assert.isFalse(list.contains(x2));
        Assert.isTrue(list.contains(x3));

        list.remove(x3);

        Assert.isTrue(list.contains(x1));
        Assert.isFalse(list.contains(x2));
        Assert.isFalse(list.contains(x3));
    }
}
