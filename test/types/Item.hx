package types;

import linkedlist.LinkedListItem;


class Item implements LinkedListItem
{
    public final value: Int;


    public function new(value: Int)
    {
        this.value = value;
    }


    public function toString(): String
    {
        return Std.string(value);
    }
}
