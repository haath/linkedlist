package linkedlist;


@:autoBuild(linkedlist.macro.LinkedListItemBuilder.build())
interface LinkedListItem
{
    @:dox(hide) @:noCompletion var __next: Any;
    @:dox(hide) @:noCompletion var __prev: Any;
    @:dox(hide) @:noCompletion var __linkedList: Any;
}
