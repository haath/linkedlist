package linkedlist.types;


enum IterationOrder
{
    /**
     * First-In-First-Out iteration; a.k.a queue.
     */
    FIFO;

    /**
     * Last-In-First-Out iteration; a.k.a stack.
     */
    LIFO;
}
