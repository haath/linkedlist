package linkedlist.macro;

import haxe.macro.Type.ClassType;
#if macro
import haxe.macro.Context;
import haxe.macro.Expr.Field;
using haxe.macro.ExprTools;


class LinkedListItemBuilder
{
    public static macro function build(): Array<Field>
    {
        var fields: Array<Field> = Context.getBuildFields();

        // first check if this type extends another type which has implemented LinkedListItem
        if (parentAlreadyImplements(Context.getLocalClass().get()))
        {
            // do nothing
            return fields;
        }

        // add __next field
        fields.push({
            name: '__next',
            meta: [
                { name: ':noCompletion', pos: Context.currentPos() }
            ],
            pos: Context.currentPos(),
            kind: FVar(TPath({
                pack: [],
                name: 'Any'
            })),
            access: [ APublic ]
        });

        // add __prev field
        fields.push({
            name: '__prev',
            meta: [
                { name: ':noCompletion', pos: Context.currentPos() }
            ],
            pos: Context.currentPos(),
            kind: FVar(TPath({
                pack: [],
                name: 'Any'
            })),
            access: [ APublic ]
        });

        // add __prev __linkedList
        fields.push({
            name: '__linkedList',
            meta: [
                { name: ':noCompletion', pos: Context.currentPos() }
            ],
            pos: Context.currentPos(),
            kind: FVar(TPath({
                pack: [],
                name: 'Any'
            })),
            access: [ APublic ]
        });

        return fields;
    }


    static function parentAlreadyImplements(type: ClassType): Bool
    {
        if (type.superClass != null)
        {
            for (int in type.superClass.t.get().interfaces)
            {
                if (int.t.get().name == 'LinkedListItem')
                {
                    return true;
                }
            }

            return parentAlreadyImplements(type.superClass.t.get());
        }

        return false;
    }
}
#end
