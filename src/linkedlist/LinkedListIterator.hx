package linkedlist;

import linkedlist.types.IterationOrder;
import linkedlist.LinkedListItem;


/**
 * Type implementating `Iterator<T>`, for loops over `LinkedList`.
 *
 * Warning: this object should not be touched by the user, and it is recommended to only iterate over lists
 * using for-loops.
 */
class LinkedListIterator<T: LinkedListItem>
{
    @:allow(linkedlist.LinkedList) var iter: T;
    @:allow(linkedlist.LinkedList) var order: IterationOrder;


    @:allow(linkedlist.LinkedList)
    function new(order: IterationOrder)
    {
        iter = null;
        this.order = order;
    }


    public inline function hasNext(): Bool
    {
        return iter != null;
    }


    public inline function next(): T
    {
        var next: T = iter;
        iter = switch order
        {
            case FIFO: iter.__prev;
            case LIFO: iter.__next;
        }
        return next;
    }

}
