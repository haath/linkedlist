package linkedlist;

import linkedlist.types.IterationOrder;

/**
 * Linked list class.
 */
/**
 * Implementation note:
 * New items are always added to the head, regardless of iteration order.
 * This means that the iteration order only affects the `pop()` and iterator methods.
 * It would have been simpler to only influence whether items are pushed to the head or to the tail,
 * but that way it would not have been possible to change the iteration order on the fly; now it is.
 */
class LinkedList<T: LinkedListItem>
{
    public var length(default, null): Int;
    var head: T;
    var tail: T;
    var iterators: Array<LinkedListIterator<T>>;
    var iterOrder: IterationOrder;


    public inline function new(iterOrder: IterationOrder = FIFO)
    {
        length = 0;
        head = null;
        tail = null;
        iterators = [];
        this.iterOrder = iterOrder;
    }


    /**
     * Removes and returns the next item in the linked list.
     *
     * This method will lead to an error if the list is empty, so it should be checked
     * first using `isEmpty()`.
     *
     * @return the item that was removed
     */
    public inline function pop(): T
    {
        if (head == null)
        {
            throw 'called pop() on empty list';
        }

        var ret: T = head;

        switch iterOrder
        {
            case _ if (head == tail):
                // popping the last element
                ret = head;
                head = null;
                tail = null;

            case FIFO:
                ret = tail;
                tail = tail.__prev;
                tail.__next = null;

            case LIFO:
                ret = head;
                head = head.__next;
                head.__prev = null;
        }

        // check if currently iterating on this item
        fixIterators(ret);

        ret.__next = null;
        ret.__prev = null;
        ret.__linkedList = null;

        length--;

        return ret;
    }


    /**
     * Adds an item to the top of the linked list.
     *
     * @param item the item to add
     */
    public inline function push(item: T)
    {
        item.__next = head;
        item.__linkedList = this;

        if (head == null)
        {
            // empty list
            head = item;
            tail = item;
        }
        else
        {
            head.__prev = item;
            head = item;
        }

        length++;
    }


    /**
     * Remove an item that may be in any position in the list.
     *
     * @param item the item to remove
     */
    public inline function remove(item: T)
    {
        if (item.__linkedList != this)
        {
            throw 'item to remove is not in this list';
        }

        if (head == tail)
        {
            // last item
            head = null;
            tail = null;

            length--;
        }
        else if ((item == head && iterOrder == LIFO)
              || (item == tail && iterOrder == FIFO))
        {
            // next item
            pop();
        }
        else if (item.__next == null)
        {
            // tail item
            var prev: T = item.__prev;
            prev.__next = null;
            tail = prev;

            length--;
        }
        else if (item.__prev == null)
        {
            // head item
            var next: T = item.__next;
            next.__prev = null;
            head = next;

            length--;
        }
        else
        {
            // middle item
            var prev: T = item.__prev;
            var next: T = item.__next;

            prev.__next = next;
            next.__prev = prev;

            length--;
        }

        // check if currently iterating on this item
        fixIterators(item);

        item.__next = null;
        item.__prev = null;
        item.__linkedList = null;
    }


    /**
     * Returns `true` if the list is currently empty.
     */
    public inline function isEmpty(): Bool
    {
        return head == null;
    }


    /**
     * Checks if a given item is in the list.
     *
     * @param item the item to check
     * @return `true` if the list contains the item, otherwise `false`
     */
    public inline function contains(item: T): Bool
    {
        return item.__linkedList == this;
    }


    /**
     * Changes the iteration order of the list.
     *
     * Calling this method in a for-loop will not affect the current iteration.
     * However, calling this method in a for-loop, and then calling `pop()` or `remove()` may lead to an undefined state.
     */
    public inline function reverse()
    {
        iterOrder = switch iterOrder
        {
            case FIFO: LIFO;
            case LIFO: FIFO;
        }
    }


    /**
     * Returns an iterator object to enable iteration in for-loops.
     *
     * Note: the iteration object returned should not be stored by the user, since it is being reused internally.
     *
     * @return the iterator object
     */
    @:noCompletion public function iterator(): Iterator<T>
    {
        var iterator: LinkedListIterator<T> = null;
        for (iter in iterators)
        {
            if (!iter.hasNext())
            {
                iterator = iter;
                break;
            }
        }
        if (iterator == null)
        {
            iterator = new LinkedListIterator(iterOrder);
            iterators.push(iterator);
        }

        iterator.iter = switch iterOrder
        {
            case FIFO: tail;
            case LIFO: head;
        }
        iterator.order = iterOrder;

        return iterator;
    }


    /**
     * Checks all currently ongoing iterators, and if any is currently pointing to the given item,
     * it is advanced to the next one.
     *
     * This should be called from `pop()` or `remove()`, to ensure that iterations don't break if called from a for-loop.
     *
     * @param item the item to check
     */
    inline function fixIterators(item: T)
    {
        for (iter in iterators)
        {
            if (iter.hasNext() && iter.iter == item)
            {
                iter.next();
            }
        }
    }
}
